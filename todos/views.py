from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from todos.models import TodoList, TodoItem

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todo_lists/list.html"


class TodolistDetailView(DetailView):
    model = TodoList
    template_name = "todo_lists/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name: str = "todo_lists/create.html"
    fields = ["name"]
    # success_url = reverse_lazy("todo_list_list")

    def get_success_url(self):
        return reverse("todo_list_detail", args=[self.object.pk])


class TodoListUpdateView(UpdateView):
    model = TodoList
    fields = ["name"]
    template_name = "todo_lists/update.html"
    # context_object_name = "todo_list_update"

    # def form_valid(self, form):
    #     form.instance.author = self.request.user
    #     return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.pk])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todo_lists/delete.html"

    def get_success_url(self):
        return reverse("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "todo_items/create.html"

    # use the foreignkey attribute to complete redirect args
    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.pk])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "todo_items/update.html"

    def get_success_url(self):
        return reverse("todo_list_detail", args=[self.object.list.pk])
